# -*- coding: utf-8 -*-

from trytond.pool import Pool
from trytond.report import Report
from trytond.transaction import Transaction

from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

__all__ = ['FormalitiesReport']

class FormalitiesReport(Report):
    'Nursing Report'
    __name__ = 'formalities.reports'


    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()

        Company = Pool().get('company.company')
        Formalities = Pool().get('formalities_caac.formalities')

        User = Pool().get('res.user')
        user = User(Transaction().user)
        
        context = super(FormalitiesReport, cls).get_context(records, data, name)
        
        data = context['data']
        print(data)
        if data:
            start = data['start'] 
            end = data['end'] 
            print("Fecha",start, end)
           
            formalities = Formalities.search([('formality_date','>=',start),
                                                ('formality_date','<=',end)])

            context['values'] = {'educational inclusion':0, 'gestion de turnos':0, 'tramite para med':0,
                                    'dni':0, 'subsidio':0, 'salario':0, 'becas':0, 'certificado de discapacidad':0,
                                    'insercion laboral':0, 'anses':0, 'gestion de servicios':0, 'otros':0}
            
            context['institutions'] = []

            for formality in formalities:
                if formality.type_formalitie == 'e':
                    context['values']['educational inclusion'] += 1 
                elif formality.type_formalitie == 's':
                    context['values']['gestion de servicios'] += 1
                elif formality.type_formalitie == 'o':
                    context['values']['otros'] += 1
                elif formality.type_formalitie == 't':
                    context['values']['gestion de turnos'] += 1
                    context['institutions'].append(formality.formality_institution)
                elif formality.type_general_formality == 'm':
                    context['values']['tramite para med'] += 1  
                elif formality.type_general_formality == 'dni':
                    context['values']['dni'] += 1  
                elif formality.type_general_formality == 'sub':
                    context['values']['subsidio'] += 1 
                elif formality.type_general_formality == 'sal':
                    context['values']['salario'] += 1  
                elif formality.type_general_formality == 'bec':
                    context['values']['becas'] += 1 
                elif formality.type_general_formality == 'cd':
                    context['values']['becas'] += 1  
                elif formality.type_general_formality == 'lab':
                    context['values']['insercion laboral'] += 1 
                elif formality.type_general_formality == 'anses':
                    context['values']['anses'] += 1  
            
            context['start_date'] = start - timedelta(hours=3) 
            context['end_date'] = end - timedelta(hours=3)
            context['current_date'] = date.today()

        return context


