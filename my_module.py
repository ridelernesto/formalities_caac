# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSQL, fields
from dateutil.relativedelta import relativedelta
from trytond.pyson import Eval, Bool, Or
from datetime import datetime, timedelta, date

__all__ = ['Formalities']


class Formalities(ModelSQL,ModelView):
    
    'Formalities CAAC'

    __name__ = 'formalities_caac.formalities'
    
    user = fields.Many2One('gnuhealth.patient','Solicitante', required=True)

    formality_date = fields.Date('Fecha', help='Date of formality', required=True)

    formality_institution = fields.Many2One('gnuhealth.institution', 'Institucion',
        states={'invisible': Eval('type_formalitie') != 't'})

    type_service = fields.Selection([
        (None , ''),
        ('sr', 'Silla de rueda'),
        ('pr', 'Prótesis'),
        ('an', 'Anteojos'),
        ('ot', 'Otro')
        ], 'Tipo de servicio', states={'invisible': Eval('type_formalitie') != 's'}, sort=False)

    type_general_formality = fields.Selection([
        (None , ''),
        ('m', 'Medicación'),
        ('dni', 'DNI'),
        ('sub', 'Subsidio'),
        ('sal', 'Salario'),
        ('bec', 'Becas'),
        ('cd', 'Certificado de discapacida'),
        ('lab', 'Inserción laboral'),
        ('anses', 'Servicios provisionales (ANSES/CUIL)'),
        ('ot', 'Otro')
        ], 'Tipo de tramite general', states={'invisible': Eval('type_formalitie') != 'g'}, sort=False)    
    
    type_formalitie = fields.Selection([
        (None , ''),
        ('g', 'Tramite General'),
        ('t', 'Gestion de Turno'),
        ('e', 'Gestion para la inclusion educativa'),
        ('s', 'Gestion de Servicios'),
        ('a', 'Asesoramiento y Patrocinio legal'),
        ('o', 'Otro')
        ], 'Tipo de tramite', required=True, sort=False)
    
    notes = fields.Text('Notas')

    @staticmethod
    def default_formality_date():
        now = datetime.now()
        return now


