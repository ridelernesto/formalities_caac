from trytond.pool import Pool
from .my_module import *
from .report import *
from .wizard import *

def register():
    Pool.register(
        Formalities,
        CreateFormalitiesReportsStart,
        module='formalities_caac', type_='model')
    Pool.register(
        FormalitiesReport,
        module='formalities_caac', type_='report')
    Pool.register(
        CreateFormalitiesReportsWizard,
        module='formalities_caac', type_='wizard')



