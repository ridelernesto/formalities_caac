# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSQL, fields
from dateutil.relativedelta import relativedelta
from trytond.pyson import Eval, Bool, Or
from datetime import datetime, timedelta, date



__all__ = ['GroupsActivities']


class GroupsActivities(ModelSQL,ModelView):
    
    'Groups Activities'

    __name__ = 'gnuhealth.groups_activities'

    name_activity = fields.Char('Nombre del taller')
    
    responsible = fields.Many2One('gnuhealth.patient','Responsable del taller', required=True)

    teachers = fields.Many2Many('gnuhealth.healthprofessional', 'name', 'name', string='Talleristas')

    participants = fields.Many2Many('gnuhealth.patient', 'name', 'name', string='Participantes')
    
    notes = fields.Text('Descripción')

    calling = fields.Boolean("calling")

    @classmethod
    def __setup__(cls):
        super(GroupsActivities, cls).__setup__()

        cls._buttons.update({
            'call': {'visible': True}})

    @classmethod
    @ModelView.button
    def call(cls, records):
    
        #TODO llamar a un wizard
        print("deberia tener un emergente")

    

