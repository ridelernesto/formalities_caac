from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 
from trytond.pyson import Eval, Not, Bool, Equal

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta

__all__ = ['CreateFormalitiesReportsStart',
           'CreateFormalitiesReportsWizard']
# modelview es de vista


class CreateFormalitiesReportsStart(ModelView):    
    'Nursing Report Start'
    __name__ = 'formalities.reports.start'
    
    report_ = fields.Selection([
        ('create_formalities_report','Reporte Estadistico Tramites'),
         ],'Report',required=True,sort=False)
    start_date = fields.Date('Fecha de inicio', required=True)
    end_date = fields.Date('Fecha de fin', required=True)

    @staticmethod
    def default_report_():
        return 'create_formalities_report'


class CreateFormalitiesReportsWizard(Wizard):
    'Nursing Report Wizard'
    __name__ = 'formalities.reports.wizard'
    
    @classmethod
    def __setup__(cls):
        super(CreateFormalitiesReportsWizard,cls).__setup__()
        pass
    
    start = StateView('formalities.reports.start',
                      'formalities_caac.create_formalities_report_start_view',[
                        Button('Cancelar','end','tryton-cancel'),
                        Button('Imprimir reporte','prevalidate','tryton-ok',default=True),
                       ])
    
    prevalidate = StateTransition()
    
    create_formalities_report =\
        StateAction('formalities_caac.act_formalities_report')
    
    
    def default_start(self,fields):
        return{
            'end_date':datetime.now()
            }
    
    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            pass
        return self.start.report_

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date
        return {
            'start':start, 
            'end':end 
            }
        
    def do_create_formalities_report(self, action):
        data = self.fill_data()
        
        return action, data
    
